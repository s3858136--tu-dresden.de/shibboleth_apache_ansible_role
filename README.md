shibboleth_apache_ansible_role
=========

This role is a simple configuration setup for Shibboleth served by Apache2. It is usable,
but is probably more like some kind of documentation about the install routine.

Requirements
------------

An existing TLS server certificate MUST be available and referenced within the role
variables. As well, this Ansible role was tested with Debian 10 (Buster).


Example Playbook
----------------
```yaml
- hosts: domain.tld
  vars:
    apache_fqdn: "domain.tld"
    apache_admin_email: "admin@{{ apache_fqdn }}"

    apache_tls_cert_ca_chain: "/etc/apache2/certs/{{ apache_fqdn }}/CA.crt"
    apache_tls_cert_key: "/etc/apache2/certs/{{ apache_fqdn }}/private/private.key"
    apache_tls_cert_file: "/etc/apache2/certs/{{ apache_fqdn }}/server.crt"

  roles:
     - shibboleth_apache_ansible_role
```
